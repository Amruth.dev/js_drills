
function carModelAlphabetically(inventory){
    inventory.sort((a, b) => a.car_model.localeCompare(b.car_model));
    return inventory
}

module.exports=carModelAlphabetically
