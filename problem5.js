
function carsOlderThan2000(inventory){
    let olderCars=[]
    for(let i=0;i<inventory.length;i++){
        let cars=inventory[i]
        if(cars.car_year<2000){
            olderCars.push(cars)
        }
    }
    return olderCars.length
}

module.exports=carsOlderThan2000

